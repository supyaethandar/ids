<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Su Pyae',
            'email' => 'supyaethandar@yahoo.com',
            'password' => bcrypt('supyae'),
            'user_type' => 1
        ]);

        DB::table('users')->insert([
            'name' => 'AKN',
            'email' => 'akn@yahoo.com',
            'password' => bcrypt('akn'),
            'user_type' => 1
        ]);
    }
}
