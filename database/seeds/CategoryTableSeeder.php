<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            'laravel issues',
            'mysql issues',
            'php issues',
            'oracle issues',
            'nginx issues'
        );

        foreach ($data as $d=>$value) {
            DB::table('category')->insert([
                'name' => $value
            ]);
        }
    }
}
