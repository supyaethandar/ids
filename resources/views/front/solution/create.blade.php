@include('common.header')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="head-title"> Solution</h4>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">

            <div class="panel">

                <div class="panel-heading">
                    <div class="well">
                        YOUR ISSUE
                        <div class="panel-footer">
                            {!! $issues->title !!}

                        </div>
                        <div class="panel-body">
                            <p>
                                {!! $issues->detail  !!}
                            </p>
                        </div>
                    </div>
                </div>


                <!-- /.col-lg-4 -->

                <div class="panel panel-default">

                    @include('common.error')

                    <div class="panel-body">

                        <div class="col-lg-9">
                            {!! Form::open(array('route'=> array('solutions/store'),
                        'method'=>'POST', 'files' => true, 'class'=> 'form-horizontal form-label-left','id'=> 'demo-form2')) !!}

                            <div class="form-group">
                                <label><h4> Please give your solutions in detail </h4></label>
                                <div class="box-body pad">
                                    <textarea id="editor1" name="solution" rows="10" cols="80">

                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><h4>Reference Link</h4> <span style="color: red;"></span> </label>
                                <textarea class="form-control" rows="6" name="reference_link"></textarea>
                            </div>

                            <input type="hidden" name="user_id" value=1>
                            <input type="hidden" name="issue_id" value="{{ $issues->id }}">

                            <button type="submit" class="btn btn-primary" style="float: left;">Save</button>

                            {!! Form::close() !!}

                            &nbsp;&nbsp;

                            <button type="reset" class="btn btn-warning" onclick="history.back()">CANCEL</button>
                        </div>

                        <!-- /.col-lg-6 (nested) -->

                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

    <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
    <script src="/shared/js/jQuery-2.1.4.min.js"></script>

    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1');
            //bootstrap WYSIHTML5 - text editor
        });
    </script>


@include('common.footer')