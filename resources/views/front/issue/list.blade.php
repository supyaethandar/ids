@include('common.header')
        <!--
<script>
    function toggler(toggerid) {
        $('#' + toggerid).toggle();
    }
</script>
{{-- usage in => onclick="javascript:toggler('collapse{{$key}}'--}}
        -->
<script src="/shared/js/jQuery-2.1.4.min.js" xmlns="http://www.w3.org/1999/html"></script>

<script>

    $(document).ready(function () {

        $('#search').change(function () {
            var path = window.location.origin;
            var category_id = $(this).val();

            $.get(path + '/issues/search/' + category_id, function (data) {
                $("#result").html(data);
            });

        });
    });

</script>


<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="head-title"> {{ ucfirst($title) }} </h4>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- search panel -->

    <div class="row">
        <div class="col-lg-12">
            <div class="jumbotron" style="padding: 5%;">
                <span style="float: left;color: #ffffff;"> Search By Issue Category : </span>
                <div class="col-lg-6">
                    {!! Form::select('category_id',array_pluck($categories, 'name', 'id'), null, ['class' => 'form-control', 'id' => 'search', 'onchange' => 'test()']) !!}
                </div>

                <input type="reset" value="RESET" class="btn btn-adn" onclick="location.reload(true)">
            </div>
        </div>

        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default" id="result">

                @if(Auth::check())

                    <a href="{{ url('issues/new') }}">
                        <button class="btn btn-default" id="new"> ADD NEW ISSUE</button>
                    </a>

                    @endif
                            <!-- .panel-heading -->
                    <div class="panel-body">
                        <div class="panel-group" id="accordion">

                            @foreach($issues as $key=>$issue)

                                <div class="panel">

                                    <div class="panel-heading">
                                        <div class="well">
                                            <h3 class="panel-title">
                                                <span style="color: #337AB7"> Problem :</span> {!! $issue->title !!}
                                            </h3>
                                            <br/>


                                            <h5>{!! $issue->detail !!}</h5>

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}"
                                               class="btn btn-success btn-sm">
                                                ANSWER
                                            </a>

                                            <a href="{{ action($controller.'@edit', ['id' => $issue->id]) }}"
                                               class="btn btn-warning btn-sm">
                                                EDIT
                                            </a>
                                            @if (Auth::check())
                                                @if (Auth::user()->user_type == 1)

                                                    <a href="{{ url('issues/destroy', ['id' => $issue->id]) }}"
                                                       class="btn btn-danger btn-sm">
                                                        DELETE
                                                    </a>
                                                @endif
                                            @endif

                                            <div style="padding-bottom: 2%"></div>

                                            <div id="collapse{{$key}}" class="panel-collapse collapse">

                                                <div class="solutions">

                                                    @if (sizeof($issue->solutions) > 0)

                                                        <div class="panel-primary">
                                                            @foreach($issue->solutions as $skey=>$solution)
                                                                <div style="padding-bottom: 1px;padding-top: 2px">
                                                                    <h5 style="color: #337AB7">
                                                                        Solution {{ $skey+1  }} </h5>
                                                                </div>

                                                                <div class="well" style="background-color: #ffffff">
                                                                    {!!  $solution->solution !!} <br/>

                                                                    @if($solution->reference_link != '')
                                                                        <span class="success"> Reference Link : </span> {!! $solution->reference_link !!}
                                                                    @endif

                                                                    <br/>

                                                                    @if (Auth::check())
                                                                        <div style="float: right">
                                                                            <a href="{{ url('solutions/edit', ['issue_id' => $issue->id, 'solution_id' => $solution->id]) }}">
                                                                                <button class="btn btn-default">
                                                                                    Edit Solution
                                                                                </button>
                                                                            </a>
                                                                        </div>
                                                                    @endif

                                                                    &nbsp;&nbsp;

                                                                    @if (Auth::check())
                                                                        @if (Auth::user()->user_type == 1)
                                                                            <div style="float: right">
                                                                                <a href="{{ url('solutions/destroy', ['solution_id' => $solution->id]) }}">
                                                                                    <button class="btn btn-danger">
                                                                                        Delete Solution
                                                                                    </button>
                                                                                </a>
                                                                            </div>

                                                                        @endif
                                                                    @endif

                                                                </div>


                                                            @endforeach

                                                        </div>

                                                    @else
                                                        <div style="padding-bottom: 5;%;">
                                                            <div class="panel-primary">
                                                                <span> Currently There is no solution for this issue ! </span>
                                                            </div>
                                                        </div>
                                                    @endif


                                                    @if (Auth::check())
                                                        <div>
                                                            <a href="{{ url('solutions/new',['issue_id' => $issue->id]) }}" class="btn btn-default btn-block">
                                                                <b>ADD SOLUTION</b>
                                                            </a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>


                                        </div>

                                    </div>

                                </div>

                                <div style="border-bottom: 2px solid steelblue"></div>

                            @endforeach

                        </div>
                    </div>
                    <div align="center">
                        <?php echo $issues->render(); ?>
                    </div>
            </div>
        </div>
    </div>
</div>


@include('common.footer')
