<div class="panel-body">
    <div class="panel-group" id="accordion">

        @if (sizeof($issues) > 0)
            <a href="{{ url('pdf', ['category_id' => $category_id]) }}" class="btn btn-primary"> Download PDF</a>

            @foreach($issues as $key=>$issue)

                <div class="panel">

                    <div class="panel-heading">
                        <div class="well">
                            <h3 class="panel-title">
                                <span style="color: #337AB7"> Problem :</span> {!! $issue->title !!}
                            </h3>
                            <br/>


                            <h5>{!! $issue->detail !!}</h5>

                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}">
                                <button class="btn btn-primary btn-sm"> ANSWER</button>
                            </a>
                            <a href="{{ action($controller.'@edit', ['id' => $issue->id]) }}"
                               class="btn btn-warning btn-sm">
                                EDIT
                            </a>

                            @if (Auth::check())
                                @if (Auth::user()->user_type == 1)

                                    <a href="{{ url('issues/destroy', ['id' => $issue->id]) }}"
                                       class="btn btn-danger btn-sm">
                                        DELETE
                                    </a>
                                @endif
                            @endif

                            <div style="padding-bottom: 2%"></div>

                            <div id="collapse{{$key}}" class="panel-collapse collapse">

                                <div style="border-radius: 10px; padding: 10px">
                                    <div class="panel-primary">
                                        @foreach($issue->solutions as $skey=>$solution)
                                            <div style="padding-bottom: 1px;padding-top: 2px">
                                                <h5 style="color: #337AB7">Solution {{ $skey+1  }} </h5>
                                            </div>

                                            <div class="well" style="background-color: #ffffff">
                                                {!!  $solution->solution !!} <br/>

                                                @if($solution->reference_link != '')
                                                    <span class="success"> Reference Link : </span> {!! $solution->reference_link !!}
                                                @endif

                                                <br/>

                                                @if (Auth::check()) {
                                                <div style="float: right">
                                                    <a href="{{ url('solutions/edit', ['issue_id' => $issue->id, 'solution_id' => $solution->id]) }}">
                                                        <button class="btn btn-default">
                                                            Edit Solution
                                                        </button>
                                                    </a>
                                                </div>
                                                @endif

                                                &nbsp;&nbsp;

                                                @if (Auth::check())
                                                    @if (Auth::user()->user_type == 1)
                                                        <div style="float: right">
                                                            <a href="{{ url('solutions/destroy', ['solution_id' => $solution->id]) }}">
                                                                <button class="btn btn-danger">
                                                                    Delete Solution
                                                                </button>
                                                            </a>
                                                        </div>

                                                    @endif
                                                @endif

                                            </div>


                                        @endforeach

                                    </div>

                                    @if (Auth::check())
                                        <div>
                                            <a href="{{ url('solutions/new',['issue_id' => $issue->id]) }}" class="btn btn-default btn-block">
                                                <b>ADD SOLUTION</b>
                                            </a>

                                        </div>
                                    @endif
                                </div>
                            </div>


                        </div>

                    </div>

                </div>

                {{--<div style="border-bottom: 2px solid steelblue"></div>--}}

            @endforeach

        @else
            <div class="panel">
                <h3>
                    <span style="color: #337AB7;text-align: center"> NO ISSUE FOUND !</span>
                </h3>

            </div>

        @endif

    </div>
</div>

@if (sizeof($issues) > 0)
    <div align="center">
        <?php echo $issues->render(); ?>
    </div>
@endif