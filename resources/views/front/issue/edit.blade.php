@include('common.header')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="head-title"> {{ ucfirst($title) }} </h4>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-default">
                </div>
                @include('common.error')
                <div class="panel-body">

                    <div class="col-lg-9">
                        {!! Form::open(array('action'=> [$controller.'@update', $data[0]->id],
                    'method'=>'PUT', 'files' => true, 'class'=> 'form-horizontal form-label-left','id'=> 'demo-form2')) !!}
                        <div class="form-group">
                            <label><h4>Your Issue Title</h4></label>
                            <input class="form-control" name="title" value="{!! $data[0]->title !!}">
                            <p class="help-block">Example. Laravel5.2 No Input file specified :) .</p>
                        </div>
                        <div class="form-group">
                            <label><h4>Choose Your Issue Category </h4></label>

                            {!! Form::select('category_id',array_pluck($dropdown, 'name', 'id'), $data[0]->category[0]->id, ['class' => 'form-control']) !!}

                        </div>

                        <div class="form-group">
                            <label><h4> Please give your issues in detail </h4></label>

                            <div class="box-body pad">
                                         <textarea id="editor1" name="detail" rows="10" cols="80">
                                             {!! $data[0]->detail !!}

                                         </textarea>
                            </div>
                        </div>
                        <input type="hidden" name="user_id" value=1>

                        <button type="submit" class="btn btn-primary">UPDATE</button>
                        <button class="btn btn-warning" onclick="history.back()">CANCEL</button>
                        {!! Form::close() !!}
                    </div>

                    <!-- /.col-lg-6 (nested) -->

                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script src="/shared/js/jQuery-2.1.4.min.js"></script>

<script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
    });
</script>


@include('common.footer')