@include('common.header')

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h4 class="head-title"> {{ ucfirst($title) }} </h4>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    @if(Auth::check())
                        @if (Auth::user()->user_type != 1 && $title != 'users')

                            {!! Html::linkAction($controller.'@create','Add New',null, ['class' => 'btn btn-round btn-primary']) !!}

                        @endif
                    @endif

                    <div style="margin-bottom: 1%"></div>
                    <table width="100%" class="table table-responsive" id="datatable">
                        <thead>
                        <tr>
                            @foreach($column as $col)
                                @if ($col != 'password')
                                    <th>  {{ ucfirst($col) }}</th>
                                @endif
                            @endforeach
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data as $data)
                            <tr>
                                @foreach($column as $col)
                                    @if ($col != 'password')
                                        <td>{{ $data->$col }}</td>
                                    @endif

                                @endforeach
                                <td>
                                    @if(Auth::check())
                                        @if (Auth::user()->user_type == 1)
                                            {!! Html::linkAction($controller.'@edit','Edit',array('id'=> $data->id), ['class' => 'btn btn-round btn-success']) !!}
                                            &nbsp;

                                            {!! Html::linkRoute($title.'/destroy','Delete',['id' => $data->id], ['class' => 'btn btn-round btn-danger']) !!}
                                        @endif
                                    @endif

                                </td>

                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('common.footer')