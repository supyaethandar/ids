</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="/shared/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/shared/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/shared/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="/shared/vendor/raphael/raphael.min.js"></script>
<script src="/shared/vendor/morrisjs/morris.min.js"></script>
<script src="/shared/data/morris-data.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/shared/dist/js/sb-admin-2.js"></script>

</body>

</html>
