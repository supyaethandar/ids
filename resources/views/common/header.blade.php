<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HVC ISSUES DOCUMENTATION SYSTEM</title>

    <!-- Bootstrap Core CSS -->
    <link href="/shared/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/shared/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    {{--<link href="/shared/dist/css/sb-admin-2.css" rel="stylesheet">--}}

    <link href="/shared/dist/css/custom.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/shared/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/shared/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">ISSUES DOCUMENTATION SYSTEM</a>
        </div>
        <!-- /.navbar-header -->

        <header class="clearfix">
            <div class="container">
                <div class="header-left">
                    <h1></h1>
                </div>
                @if (Auth::check())
                    <div class="header-right">
                        <label for="open">
                            <span class="hidden-desktop"></span>
                        </label>
                        <input type="checkbox" name="" id="open">

                        <nav>
                            <a href="{{ url('/') }}">Home</a>
                            <a href="{!! url('users/') !!}">Users</a>
                            <a href="{!! url('/issues') !!}">Issues</a>
                            <a href="{!! url('category/') !!}">Categories</a>
                            <a href="#">About</a>
                        </nav>
                    </div>
                    <div>
                        <ul class="nav navbar-top-links navbar-right">
                            <!-- /.dropdown -->
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">

                                    <li><a href="{{ action('UserController@edit', ['id' => Auth::user()->id]) }}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                                    </li>
                                </ul>
                                <!-- /.dropdown-user -->
                            </li>
                            <!-- /.dropdown -->
                        </ul>

                    </div>
                @else
                    <div style="margin-top: 1%;margin-bottom: 2%;padding: 2%">
                        <ul class="nav navbar-top-links navbar-right">
                            <a href="{{ url('register') }}">
                                <button class="btn btn-default"> REGISTER</button>
                            </a>
                            <a href="{{ url('login') }}">
                                <button class="btn btn-default"> LOGIN</button>
                            </a>
                        </ul>

                    </div>
                @endif


            </div>
        </header>

        <!-- /.navbar-static-side -->
    </nav>