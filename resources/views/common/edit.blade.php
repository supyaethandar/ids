@include('common.header')

<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <h4 class="head-title"> {{ ucfirst($title) }} </h4>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <!-- /.panel-heading -->
                <div class="panel-body">
                    {!! Form::open(array('action' => [$controller.'@update', $data->id],
                        'method'=>'PUT', 'class'=> 'form-horizontal form-label-left','id'=> 'demo-form2')) !!}

                    @foreach($column as $col)

                        <div class="form-group">
                            <!--- label -->
                                @if ($col == 'last_login' || $col == 'password')
                                    &nbsp;

                                @else

                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                       for="first-name">{{ ucfirst($col) }} <span class="required">*</span>
                                </label>
                                @endif

                                        <!--- Input  -->
                                @if($col == 'user_type')
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label style="margin:0 10px 10px 0;">

                                                {!! Form::radio('user_type','1', ($data->user_type == 1? true: false)) !!}
                                                <span style="padding:0 0 0 5px;">Super</span>
                                            </label>
                                            <label style="margin:0 10px 10px 0;">
                                                {!! Form::radio('user_type','0', ($data->user_type == 0? true: false)) !!}
                                                <span style="padding:0 0 0 5px;">Normal</span>
                                            </label>
                                        </div>

                                    </div>
                                @elseif ($col == 'password' || $col == 'last_login')
                                    &nbsp;
                                @else
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="first-name" name="{{ $col }}" required="required"
                                               value="{{ $data->$col }}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                @endif
                        </div>

                    @endforeach


                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <input type="button" class="btn btn-danger" onclick="history.back()" value="Cancel">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>
            </div>
        </div>
    </div>
</div>
@include('common.footer')