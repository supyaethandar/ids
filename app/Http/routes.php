<?php

Route::auth();

Route::get('', 'IssueController@index');

Route::post('login', 'Auth\AuthController@postLogin');
Route::post('register', 'Auth\AuthController@postRegister');

Route::get('pdf/{id}', 'IssueController@fetchPdf');

Route::controller('/password', 'Auth\PasswordController');

Route::group(['middleware' => ['web'], 'prefix' => '/issues'], function () {
    Route::get('', ['as' => 'issues', 'uses' => 'IssueController@index']);
    Route::get('new', ['as' => 'issues/new', 'uses' => 'IssueController@create']);
    Route::get('destroy/{id}', ['as' => 'issues/destroy', 'uses' => 'IssueController@destroy']);

    Route::get('search/{id}', ['as' => 'issues/search', 'uses' => 'IssueController@search']);

    Route::resource('issues', 'IssueController');
});

Route::group(['middleware' => ['web'], 'prefix'=> 'solutions'], function() {
    Route::get('', ['as' => 'solutions/test', 'uses' => 'SolutionsController@index']);
    Route::get('new/{id}', ['as' => 'solutions/new', 'uses' => 'SolutionsController@create']);
    Route::get('edit/{issue_id}/{solution_id}', ['as' => "solutions/edit", 'uses' => 'SolutionsController@editSolution']);
    Route::put('update/{id}', ['as' => "solutions/update", 'uses' => 'SolutionsController@update']);
    Route::post('store', ['as' => 'solutions/store', 'uses' => 'SolutionsController@store']);
    
    Route::get('destroy/{id}', ['as' => 'solutions/destroy', 'uses' => 'SolutionsController@destroy']);
});


Route::group(['middleware' => ['web'], 'prefix'=> 'users'], function() {
    Route::get('', ['as' => 'users', 'uses'=>'UserController@index']);
    Route::get('new/{id}', ['as' => 'users/new', 'uses' => 'UserController@create']);

    Route::post('store', ['as' => 'users/store', 'uses' => 'UserController@store']);
    Route::get('destroy/{id}', ['as' => 'users/destroy', 'uses' => 'UserController@destroy']);

    Route::resource('users', 'UserController');
});


Route::group(['middleware' => ['web'], 'prefix'=> 'category'], function() {
    Route::get('', ['as' => 'category', 'uses'=>'CategoryController@index']);
    Route::get('new/{id}', ['as' => 'category/new', 'uses' => 'CategoryController@create']);

    Route::post('store', ['as' => 'category/store', 'uses' => 'CategoryController@store']);
    Route::get('destroy/{id}', ['as' => 'category/destroy', 'uses' => 'CategoryController@destroy']);

    Route::resource('users', 'CategoryController');
});

