<?php

namespace App\Http\Controllers;

use App\Repositories\Category\CategoryRepository;

use App\Http\Requests;

class CategoryController extends BaseController
{

    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        parent::__construct(new CategoryRepository(), new Requests\CategoryFormRequest());
    }

    public function setSetting()
    {
        parent::setSetting();
        $this->CONTROLLER_NAME = 'CategoryController';
        $this->REDIRECT_URL = 'category';
        $this->TITLE = 'category';
    }
}
