<?php namespace App\Http\Controllers;

use App\Repositories\Category\CategoryRepository;
use App\Repositories\Issues\IssuesRepository;
use App\Repositories\Solutions\SolutionsRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class SolutionsController extends BaseController
{
    /**
     * SolutionsController constructor.
     */
    public function __construct()
    {
        parent::__construct(new SolutionsRepository(), new Requests\SolutionFormRequest());
    }

    public function setSetting()
    {
        $this->TITLE = 'SOLUTIONS';
        $this->CREATE_VIEW_NAME = 'front.solution.create';
        $this->EDIT_VIEW_NAME = 'front.solution.edit';
        $this->REDIRECT_URL = 'issues';
        $this->CONTROLLER_NAME = 'SolutionsController';
    }

    public function create($issue_id = null) {
        $issues = (new IssuesRepository())->find($issue_id);

        return view($this->CREATE_VIEW_NAME, compact('issues'));
    }

    public function editSolution($issue_id, $solution_id) {

        $controller = $this->CONTROLLER_NAME;

        $issues = (new IssuesRepository())->find($issue_id);

        $solutions = (new SolutionsRepository())->find($solution_id);

        return view($this->EDIT_VIEW_NAME, compact('issues', 'solutions', 'controller'));
    }
}
