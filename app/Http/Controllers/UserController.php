<?php

namespace App\Http\Controllers;

use App\Repositories\Users\UserRepository;

use App\Http\Requests;

class UserController extends BaseController {

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        parent::__construct(new UserRepository(), new Requests\UserFormRequest());
    }

    public function setSetting()
    {
        parent::setSetting();
        $this->CONTROLLER_NAME = 'UserController';
        $this->REDIRECT_URL = 'users';
        $this->TITLE = 'users';
    }
}
