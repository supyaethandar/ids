<?php

namespace App\Http\Controllers;

use App\Repositories\Category\CategoryRepository;
use App\Repositories\Issues\IssuesRepository;
use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class IssueController extends BaseController
{
    public $pdf;
    /**
     * IssueController constructor.
     */
    public function __construct(PDF $pdf)
    {
        parent::__construct(new IssuesRepository(), new Requests\IssueFormRequest());
        $this->pdf = $pdf;
        
    }

    public function setSetting()
    {
        $this->CONTROLLER_NAME = 'IssueController';
        $this->LIST_VIEW_NAME = 'front.issue.list';
        $this->CREATE_VIEW_NAME = 'front.issue.create';
        $this->EDIT_VIEW_NAME = 'front.issue.edit';
        $this->REDIRECT_URL = 'issues';
        $this->TITLE = 'ISSUES LISTS';
    }

    public function index() {

        $title = $this->TITLE;
        $controller = $this->CONTROLLER_NAME;

        $categories = (new CategoryRepository())->getAll();

        $issues  = $this->repository->getIssuesWithSolutions();

        return view($this->LIST_VIEW_NAME, compact('title', 'issues', 'categories', 'controller'));

    }

    public function store(Request $request)
    {
        if ($validator = $this->formRequest->formValidate()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        } else {
            $input = $request->all();
            $this->repository->store($input);

            $issues = $this->repository->getLast();
            return view('front.solution.create', compact('issues'));

        }
    }
    
    public function search($category_id) {
        
        $issues = $this->repository->search($category_id);
        $controller = $this->CONTROLLER_NAME;
       
        return view('front.issue.search', compact('issues', 'category_id', 'controller'));
    }

    public function fetchPdf($category_id) {
        $current = Carbon::now();
        $issues = $this->repository->search($category_id);

        $pdfData = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';

        foreach ($issues as $key=>$issue) {
            $no = $key+1;
            $pdfData .= "<span style='font-size: 14px'>ISSUE : $no</span>";
            $pdfData .=  "<div><h5 style='color: red'>$issue->title</h5></div>";
            $pdfData .=  "<div><span style='color: #75787b;font-size: 12px;'>$issue->detail</span></div>";

            foreach ($issue->solutions as $skey=>$solution) {
                $sno = $skey+1;
                $pdfData .=  "<div style='margin-left: 20px;background-color: #f4f4f4; padding: 1%;margin-bottom: 1%;margin-top: 1%;'>";
                $pdfData .=  "<span style='font-size: 14px'>SOLUTION : $sno</span>";
                $pdfData .=  "<div><span style='font-size: 12px'>$solution->solution</span></div>";
                $pdfData .=  "<div> <span style='color: darkblue;font-size: 12px'>REFERENCE LINK :  &nbsp; $solution->reference_link </span></div>";
                $pdfData .=  "</div>";
            }

            $pdfData .=  "<div style='border-bottom: 1px solid #f4f4f4'></div>";
        }
        //$pdfData2 = mb_convert_encoding($pdfData,'HTML-ENTITIES', 'UTF-8');

        $this->pdf->getDomPDF()->load_html($pdfData);
        $output = $this->pdf->getDomPDF()->output_html();

        return $this->pdf->download($current.'document.pdf');

    }
}
