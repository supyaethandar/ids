<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommonRequest;
use App\Repositories\Category\Category;
use App\Repositories\GeneralRepositoryInterface;
use App\Repositories\Issues\IssuesRepository;
use App\Repositories\Users\UserRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

class BaseController extends Controller
{
    /**
     * BaseController constructor.
     */
    protected $LIST_VIEW_NAME;
    protected $CREATE_VIEW_NAME;
    protected $EDIT_VIEW_NAME;
    protected $REDIRECT_URL;
    protected $CONTROLLER_NAME;
    protected $COLUMN_NAME;
    protected $TITLE;
    protected $RELATION = [];

    protected $repository;
    protected $formRequest;

    public function __construct(GeneralRepositoryInterface $generalRepositoryInterface, CommonRequest $request)
    {
        $this->repository = $generalRepositoryInterface;
        $this->formRequest = $request;
        $this->setSetting();
        $this->middleware('auth.basic', ['only' => ['create', 'store', 'edit', 'update', 'delete']]);
    }

    public function setSetting()
    {
        $this->LIST_VIEW_NAME = 'common.list';
        $this->CREATE_VIEW_NAME = 'common.create';
        $this->EDIT_VIEW_NAME = 'common.edit';
    }

    public function getAll()
    {
        return $this->repository->getAll();
    }

    public function index()
    {

        $result = $this->repository->index();
        $column = $result->getData()->column;
        $data = $result->getData()->data;
        $title = $this->TITLE;
        $controller = $this->CONTROLLER_NAME;

        return view($this->LIST_VIEW_NAME, compact('column', 'data', 'title', 'controller'));
    }

    public function create()
    {
        $result = $this->repository->create();
        $column = $result->getData()->column;
        $title = $this->TITLE;

        if ($this->repository instanceof IssuesRepository) {
            $dropdown = Category::all();
            $relationKey = 'category_id';
        } else {
            $dropdown = '';
            $relationKey = '';
        }

        $controller = $this->CONTROLLER_NAME;

        return view($this->CREATE_VIEW_NAME, compact('column', 'title', 'dropdown', 'relationKey', 'controller'));
    }

    public function store(Request $request)
    {
        if ($validator = $this->formRequest->formValidate()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        } else {
            $input = $request->all();

            if ($this->repository instanceof UserRepository) {
                $password = $input['password'];
                $encrypted_password = bcrypt($password);
                unset($input['password']);
                $input['password'] = $encrypted_password;
            }

            $this->repository->store($input);

            return redirect($this->REDIRECT_URL);
        }
    }

    public function edit($id)
    {
        $controller = $this->CONTROLLER_NAME;

        $result = $this->repository->create();

        $column = $result->getData()->column;

        $data = $this->repository->find($id);
        $title = $this->TITLE;

        if ($this->repository instanceof IssuesRepository) {
            $data = $this->repository->getIssuesWithCategory($id);
            $dropdown = Category::all();
            $relationKey = 'category_id';

        } else {
            $dropdown = '';
            $relationKey = '';
        }

        return view($this->EDIT_VIEW_NAME, compact('column', 'title', 'dropdown', 'relationKey', 'controller', 'data'));
    }

    public function update(Request $request, $id)
    {
        if ($validator = $this->formRequest->editFormValidate()) {
            return redirect()->back()->withInput()->withErrors($validator->errors());
        } else {
            $input = $request->all();

            if ($this->repository instanceof UserRepository) {
                $encrypted_password = bcrypt($input['password']);
                unset($input['password']);
                $input['password'] = $encrypted_password;
            }

            $input = array_slice($input, 2);

            $this->repository->update($input, $id);

            return redirect($this->REDIRECT_URL);
        }
    }

    public function destroy($id)
    {
        $this->repository->destroy($id);

        return redirect($this->REDIRECT_URL);
    }
}
