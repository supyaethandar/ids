<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CategoryFormRequest extends CommonRequest
{
    public function __construct()
    {
        $rules = [
            'name' => 'required'
        ];


        $editRules = $rules;

        parent::__construct($rules, $editRules);
    }
}
