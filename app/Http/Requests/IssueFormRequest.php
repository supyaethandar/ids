<?php

namespace App\Http\Requests;

class IssueFormRequest extends CommonRequest
{
    public function __construct()
    {
        $rules = [
            'user_id' => 'required',
            'title' => 'required',
            'detail'  => 'required',
        ];


        $editRules = $rules;

        parent::__construct($rules, $editRules);
    }
}
