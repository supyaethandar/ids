<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SolutionFormRequest extends CommonRequest
{
    /**
     * SolutionFormRequest constructor.
     */
    public function __construct()
    {
        $rules = [
            'solution' => 'required',
        ];


        $editRules = $rules;

        parent::__construct($rules, $editRules);
    }
}
