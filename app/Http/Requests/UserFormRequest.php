<?php

namespace App\Http\Requests;


class UserFormRequest extends CommonRequest
{
    public function __construct()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required | email',
            'password'  => 'required|min:5',
            'user_type' => 'required|numeric',
        ];

        unset($rules['password']);
        $editRules = $rules;

        parent::__construct($rules, $editRules);
    }
}
