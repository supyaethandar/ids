<?php
/**
 * Created by PhpStorm.
 * User: hivelocity
 * Date: 28/2/17
 * Time: 11:00 AM
 */

namespace App\Repositories\Category;


use App\Repositories\Issues\Issues;
use Illuminate\Database\Eloquent\Model;

class Category extends Model{

    protected $table = 'category';

    protected $fillable = ['name'];

    public function issues() {
        return $this->belongsToMany(Issues::class, 'category_issues', 'category_id', 'issue_id');
    }


}