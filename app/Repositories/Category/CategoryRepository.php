<?php
/**
 * Created by PhpStorm.
 * User: hivelocity
 * Date: 28/2/17
 * Time: 1:45 PM
 */

namespace App\Repositories\Category;


use App\Repositories\GeneralRepository;

class CategoryRepository extends GeneralRepository {

    /**
     * CategoryRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(new Category());
    }
}