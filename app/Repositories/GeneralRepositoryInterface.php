<?php
/**
 * Created by PhpStorm.
 * User: hivelocity
 * Date: 28/2/17
 * Time: 1:22 PM
 */

namespace App\Repositories;


interface GeneralRepositoryInterface {

    public function getAll();

    public function index();

    public function find($id);

    public function create();

    public function store($input);

    public function update($input, $id = null);

    public function destroy($id);

}