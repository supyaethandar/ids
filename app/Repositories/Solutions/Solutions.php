<?php
/**
 * Created by PhpStorm.
 * User: hivelocity
 * Date: 28/2/17
 * Time: 10:49 AM
 */

namespace App\Repositories\Solutions;


use App\Repositories\Issues\Issues;
use App\Repositories\Users\Users;
use Illuminate\Database\Eloquent\Model;

class Solutions extends Model{
    protected $table = 'solutions';

    protected $fillable = ['issue_id', 'user_id', 'solution', 'reference_link'];

    public function issues() {
        return $this->belongsTo(Issues::class, 'issues_id');
    }

    public function users() {
        return $this->belongsTo(Users::class, 'user_id');
    }

}