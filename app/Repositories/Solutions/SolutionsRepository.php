<?php
/**
 * Created by PhpStorm.
 * User: hivelocity
 * Date: 28/2/17
 * Time: 1:46 PM
 */

namespace App\Repositories\Solutions;


use App\Repositories\GeneralRepository;

class SolutionsRepository extends GeneralRepository {

    /**
     * SolutionsRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(new Solutions());
    }
}