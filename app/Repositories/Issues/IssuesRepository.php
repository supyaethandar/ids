<?php
/**
 * Created by PhpStorm.
 * User: hivelocity
 * Date: 28/2/17
 * Time: 1:44 PM
 */

namespace App\Repositories\Issues;

use App\Repositories\GeneralRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class IssuesRepository extends GeneralRepository {

    /**
     * IssuesRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(new Issues());
    }

    public function getIssuesWithSolutions()
    {

        return $this->model->with('solutions')->orderBy('id', 'DESC')->paginate(20);
    }

    public function search($category_id)
    {

        $result = $this->model->with('category', 'solutions')
            ->whereHas('category', function ($q) use ($category_id) {
                $q->where('id', $category_id);

            })->paginate(20);

        return $result;
    }

    public function getIssuesWithCategory($id)
    {
        $result = $this->model->with('category')->where('id', $id)->get();

        return $result;
    }

    public function store($input)
    {
        $this->model->user_id = $input['user_id'];
        $this->model->title = $input['title'];
        $this->model->detail = $input['detail'];
        $this->model->save();
        $issue_id = $this->model->id;

        $this->model->category()->attach($input['category_id'], ['issue_id' => $issue_id]);
    }

    public function getLast()
    {
        return DB::table('issues')->orderBy('id', 'desc')->first();
    }

    public function update($input, $id = null)
    {
        $inputs = [
            'user_id' => Auth::user()->id,
            'title'   => $input['title'],
            'detail'  => $input['detail']
        ];
        $category_id = $input['category_id'];
        $this->model->where('id', $id)->update($inputs);
        $pivot_data = [
            'category_id' => $category_id,
            'issue_id'    => $id
        ];
        $issues = $this->model->with('category')->find($id);
        $issues->category()->sync($pivot_data);
    }

    public function destroy($id)
    {
        $this->model->category()->detach();
        parent::destroy($id);
    }

}