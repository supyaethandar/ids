<?php
/**
 * Created by PhpStorm.
 * User: hivelocity
 * Date: 28/2/17
 * Time: 10:45 AM
 */

namespace App\Repositories\Issues;


use App\Repositories\Category\Category;
use App\Repositories\Solutions\Solutions;
use App\Repositories\Users\Users;
use Illuminate\Database\Eloquent\Model;

class Issues extends Model{
    protected $table = 'issues';

    protected $fillable = ['user_id', 'title', 'detail'];

    public function users() {
        return $this->belongsTo(Users::class, 'user_id');
    }

    public function solutions() {
        return $this->hasMany(Solutions::class,'issue_id');
    }

    public function category() {
        return $this->belongsToMany(Category::class, 'category_issues', 'issue_id', 'category_id');
    }
    
}