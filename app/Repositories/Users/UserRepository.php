<?php
/**
 * Created by PhpStorm.
 * User: hivelocity
 * Date: 28/2/17
 * Time: 1:34 PM
 */

namespace App\Repositories\Users;


use App\Repositories\GeneralRepository;

class UserRepository extends GeneralRepository {
    
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(new Users());
    }

    public function addLastLogin($id) {
        $this->model->where('id', $id)->update(['last_login' => date('Y-m-d h:m:s', time())]);
    }
}