<?php
/**
 * Created by PhpStorm.
 * User: hivelocity
 * Date: 28/2/17
 * Time: 10:42 AM
 */

namespace App\Repositories\Users;


use App\Repositories\Issues\Issues;
use App\Repositories\Solutions\Solutions;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class Users extends Authenticatable{

    //use Authenticatable;
    protected $table = 'users';

    protected $fillable = ['name', 'email', 'password', 'user_type', 'last_login'];


    public function issues() {
        return $this->hasMany(Issues::class);
    }

    public function solutions() {
        return $this->hasMany(Solutions::class);
    }
}